"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template
from study import app

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Главная',
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Контакты',
        year=datetime.now().year,
        message='Связаться с нами'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='Справка',
        year=datetime.now().year,
        message='О программе.'
    )
@app.route('/Table')
def Table():
    """Renders the Table page."""
    return render_template(
        'Table.html',
        title='Таблица',
        year=datetime.now().year,
        message='Оценка налогово-фискальной нагрузки сельскохозяйственных организаций в 2019 году по районам Саратовской области ( по выручке, по начислению).'
    )
@app.route('/pril1.xlsx')
def pril1():
    """Renders the Table page."""
    return render_template(
        'pril1.xlsx.html',
        title='Таблица',
        year=datetime.now().year,
        message='Расчет налогово-фискальных потенциалов по показателям растениеводства в Саратовской области за 2019 год.'
    )
@app.route('/pril 2.xlsx')
def pril2():
    """Renders the Table page."""
    return render_template(
        'pril 2.xlsx.html',
        title='Таблица',
        year=datetime.now().year,
        message='Расчет совокупных налогово-фискальных потенциалов по районам Саратовской области за 2019 год.'
    )
@app.route('/Diff.xlsx')
def Diff():
    """Renders the Table page."""
    return render_template(
        'Diff.xlsx.html',
        title='Таблица',
        year=datetime.now().year,
        message='Дифференциация налоговой нагрузки по факторам производства. 2019'
    )
